import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.12

Window {
    width: 640
    height: 480
    visible: true

    ColumnLayout {
        anchors.fill: parent

        Text {
            id: headerPessoas
            Layout.alignment: Qt.AlignCenter
            text: qsTr("Pessoas")
        }

        ListView {
            id: listView
            Layout.alignment: Qt.AlignCenter

            Layout.preferredWidth: 75
            Layout.preferredHeight: 200

            model: ListModel{ id: model }
            delegate: Text {
                text: model.first_name + ' ' + model.last_name
            }

            Component.onCompleted: {
                var xhr = new XMLHttpRequest;
                xhr.open("GET", 'Entities.json');
                xhr.onreadystatechange = function() {
                    if (xhr.readyState === XMLHttpRequest.DONE) {
                        var data = JSON.parse(xhr.responseText);
                        model.clear();
                        for (var i in data) {
                            model.append({first_name: data[i]['first_name'], last_name: data[i]['last_name']});
                        }
                    }
                }
                xhr.send();
            }
        }
    }
}
