import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.LocalStorage 2.12
import QtQuick.Window 2.12
import "Database.js" as DB

Window {
    id: window
    visible: true
    width: 640
    height: 480

    ColumnLayout {
        anchors.fill: parent

        Text {
            id: headerPessoas
            Layout.alignment: Qt.AlignCenter
            text: qsTr("Pessoas")
        }

        ListView {
            id: listView
            Layout.alignment: Qt.AlignCenter

            Layout.preferredWidth: 75
            Layout.preferredHeight: 200

            model: ListModel {
                id: idListPessoa
                Component.onCompleted: DB.listPessoa()
            }
            delegate: Text {
                text: nome + ' ' + sobrenome
            }
        }

        Text {
            id: addTitle
            Layout.alignment: Qt.AlignCenter
            text: qsTr("Adicione Pessoas")
        }

        RowLayout {
            Layout.alignment: Qt.AlignCenter
            TextField {
                id: nomeInput
                placeholderText: 'Nome'
                placeholderTextColor: 'grey'
            }

            TextField {
                id: sobrenomeInput
                placeholderText: 'Sobrenome'
                placeholderTextColor: 'grey'
            }
        }

        Button {
            id: saveButton
            Layout.alignment: Qt.AlignCenter
            text: 'Save'
            onClicked: {
                DB.insertPessoa(nomeInput.text, sobrenomeInput.text)
                DB.listPessoa()
                nomeInput.clear()
                sobrenomeInput.clear()
            }
        }
    }

    Component.onCompleted: {
        DB.createDatabase()
    }
}
