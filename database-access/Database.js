function createDatabase()
{
    try {
        var db = LocalStorage.openDatabaseSync("pessoas", "1.0",
                                               "banco de dados de pessoas", 1000000)

        db.transaction(function (tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS pessoas (nome text,sobrenome text)')
        })
    } catch (err) {
        console.log("Error creating table in database: " + err)
    }
    return db
}

function getDatabase()
{
    try {
        var db = LocalStorage.openDatabaseSync("pessoas", "1.0",
                                               "banco de dados de pessoas", 1000000)
    } catch (err) {
        console.log("Error opening database: " + err)
    }
    return db
}

function insertPessoa(nome, sobrenome)
{
    var db = getDatabase()

    console.log(nome, sobrenome)
    db.transaction(function (tx) {
        tx.executeSql('INSERT INTO pessoas VALUES(?, ?)',
                      [nome, sobrenome])
    })

    console.log('Pessoa inserida!')
}

function listPessoa()
{
    var db = getDatabase()

    idListPessoa.clear()

    db.transaction(function (tx) {
        var results = tx.executeSql('SELECT * FROM pessoas')

        for (var i = 0; i < results.rows.length; i++) {
            console.log(results.rows.item(i).nome, results.rows.item(i).sobrenome)
            idListPessoa.append({
                 nome: results.rows.item(i).nome,
                 sobrenome: results.rows.item(i).sobrenome
            })
        }
    })
}
